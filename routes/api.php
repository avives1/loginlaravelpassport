<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'RegisterController@register');

Route::middleware('auth:api')->group(function () {
    Route::resource('alumnos','AlumnoController');
    Route::resource('alumnos_cursos','AlumnocursoController');
    Route::resource('cursos','CursoController');
    Route::resource('escuelas','EscuelaController');
    Route::resource('experiencia','ExperienciaController');
    Route::resource('formacion','FormacionController');
    Route::resource('perfiles','PerfilController');
    Route::resource('profesores','ProfesorController');
    Route::resource('profesores_cursos','ProfesorcursoController');
    Route::resource('proyectos','ProyectoController');
});

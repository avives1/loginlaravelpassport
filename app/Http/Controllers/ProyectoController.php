<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use App\Models\Proyecto;
use App\Models\Alumno;

class ProyectoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //para verlos todos
        $proyectos = Proyecto::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($proyectos->toArray(),
                'Proyectos recibidos correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //recogemos ALUMNO
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'descripcion'=> 'required',            
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $proyecto=Proyecto::create($input);
        
        return $this->sendResponse($proyecto->toArray(),
                'Proyecto creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto=Proyecto::find($id);
        
        if(is_null($proyecto)){
            return $this->sendError('Proyecto no encontrado.');
        }
        
        return $this->sendResponse($proyecto->toArray(), 'Proyecto recibido con éxito.');
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'descripcion'=> 'required',
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        
        $proyecto->nombre=$input['nombre'];
        $proyecto->descripcion=$input['descripcion'];
        $proyecto->duracion=$input['duracion'];
        $proyecto->imagen=$input['imagen'];
        $proyecto->link=$input['link'];
        $proyecto->save();
        
        return $this->sendResponse($proyecto->toArray(),'Proyecto actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto)
    {
        $proyecto->delete();
        
        return $this->sendResponse($proyecto->toArray(),'Proyecto suprimido con éxito');
    }
}

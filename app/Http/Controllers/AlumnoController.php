<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use App\Models\Alumno;
use App\Models\Perfil;


class AlumnoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //para verlos todos
        $alumnos = Alumno::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($alumnos->toArray(),
                'Alumnos recibidos correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recogemos ALUMNO
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'apellidos'=> 'required',
            'email'=> 'required',
            'fecha_nacimiento'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $alumno=Alumno::create($input);
        
        return $this->sendResponse($alumno->toArray(),
                'Alumno creado con éxito.');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alumno=Alumno::find($id);
        
        if(is_null($alumno)){
            return $this->sendError('Alumno no encontrado.');
        }
        
        return $this->sendResponse($alumno->toArray(), 'Alumno recibido con éxito.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alumno $alumno)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'apellidos'=> 'required',
            'email'=> 'required',
            'fecha_nacimiento'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.',
                    $validator->errors());
        }
        
        $alumno->nombre=$input['nombre'];
        $alumno->apellidos=$input['apellidos'];
        $alumno->telefono=$input['telefono'];
        $alumno->email=$input['email'];
        $alumno->dni=$input['dni'];
        $alumno->fecha_nacimiento=$input['fecha_nacimiento'];
        $alumno->localidad=$input['localidad'];
        $alumno->imagen=$input['imagen'];
        $alumno->curriculum=$input['curriculum'];
        $alumno->competencias=$input['competencias'];
        $alumno->aptitudes=$input['aptitudes'];
        $alumno->save();
        
        return $this->sendResponse($alumno->toArray(),'Alumno actualizado con éxito.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumno $alumno)
    {
        $alumno->delete();
        
        return $this->sendResponse($alumno->toArray(),'Alumno suprimido con éxito');
    }
    
    
    //EN CASOS ESPECIALES CREAR FUNCION Y LLAMARLA POR RUTA ESPECÍFICA
    //para verlos ordenados por: 'name', 'id', lo que sea + ascendente o descendente
        //$alumnos = Alumno::orderBy('id'/'name','asc'/'desc')->get();

        // que te devuelva uno solo buscando por 'X'
        //return Alumno::where('X=nombre','Amparo')-get();
        
        //sacarlo todo de la base de datos
        //$alumnos = DB::select('SELECT * FROM alumnos');
        
        //ordenados y paginados
        //$alumnos = Alumno::orderBy('nombre','desc')->paginate(1);
}

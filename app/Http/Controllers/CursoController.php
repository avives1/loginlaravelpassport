<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Curso;
use App\Models\Escuela;
use Validator;

class CursoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //para verlos todos
        $cursos = Curso::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($cursos->toArray(),
                'Cursos recibidos correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recogemos ALUMNO
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'descripcion'=> 'required',
            'categoria'=> 'required',
            'fecha_inicio'=> 'required',
            'fecha_fin'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $curso=Curso::create($input);
        
        return $this->sendResponse($curso->toArray(),
                'Curso creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso=Curso::find($id);
        
        if(is_null($curso)){
            return $this->sendError('Curso no encontrado.');
        }
        
        return $this->sendResponse($curso->toArray(), 'Curso recibido con éxito.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Curso $curso)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'descripcion'=> 'required',
            'categoria'=> 'required',
            'fecha_inicio'=> 'required',
            'fecha_fin'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.',
                    $validator->errors());
        }
        
        $curso->nombre=$input['nombre'];
        $curso->descripcion=$input['descripcion'];
        $curso->categoria=$input['categoria'];
        $curso->plazas=$input['plazas'];
        $curso->fecha_inicio=$input['fecha_inicio'];
        $curso->fecha_fin=$input['fecha_fin'];
        $curso->estado=$input['estado'];
        $curso->imagen=$input['imagen'];
        $curso->localidad=$input['localidad'];
        $curso->save();
        
        return $this->sendResponse($curso->toArray(),'Curso actualizado con éxito.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curso->delete();
        
        return $this->sendResponse($curso->toArray(),'Curso suprimido con éxito');
    
    }
}

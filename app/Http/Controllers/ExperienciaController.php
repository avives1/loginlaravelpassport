<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Experiencia;
use App\Models\Profesor;
use Validator;

class ExperienciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //para verlos todos
        $experiencias = Experiencia::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($experiencias->toArray(),
                'Experiencias recibidas correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recogemos ALUMNO
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'ocupacion'=> 'required',
            'duracion'=> 'required',
            'centro_educativo'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $experiencia= Experiencia::create($input);
        
        return $this->sendResponse($experiencia->toArray(),
                'Experiencia creada con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $experiencia=Experiencia::find($id);
        
        if(is_null($experiencia)){
            return $this->sendError('Experiencia no encontrada.');
        }
        
        return $this->sendResponse($experiencia->toArray(), 'Experiencia recibida con éxito.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experiencia $experiencia)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'ocupacion'=> 'required',
            'duracion'=> 'required',
            'centro_educativo'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.',
                    $validator->errors());
        }
        
        $experiencia->ocupacion=$input['ocupacion'];
        $experiencia->descripcion=$input['descripcion'];
        $experiencia->duracion=$input['duracion'];
        $experiencia->centro_educativo=$input['centro_educativo'];
        $experiencia->save();
        
        return $this->sendResponse($experiencia->toArray(),'Experiencia actualizads con éxito.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $experiencia->delete();
        
        return $this->sendResponse($experiencia->toArray(),'Experiencia suprimida con éxito');
    
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use App\Models\Escuela;
use App\Models\Perfil;

class EscuelaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //para verlos todos
        $escuelas = Escuela::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($escuelas->toArray(),
                'Escuelas recibidas correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recogemos Escuela
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'descripcion'=> 'required',
            'localidad'=> 'required',
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $escuela=Escuela::create($input);
        
        return $this->sendResponse($escuela->toArray(),
                'Escuela creada con éxito.');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $escuela=Escuela::find($id);
        
        if(is_null($escuela)){
            return $this->sendError('Escuela no encontrada.');
        }
        
        return $this->sendResponse($escuela->toArray(), 'Escuela recibida con éxito.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Escuela $escuela)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'apellidos'=> 'required',
            'telefono'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.',
                    $validator->errors());
        }
        
        $escuela->nombre=$input['nombre'];
        $escuela->descripcion=$input['descripcion'];
        $escuela->telefono=$input['telefono'];
        $escuela->localidad=$input['localidad'];
        $escuela->imagen=$input['imagen'];
        $escuela->save();
        
        return $this->sendResponse($escuela->toArray(),'Escuela actualizada con éxito.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Escuela $escuela)
    {
        $escuela->delete();
        
        return $this->sendResponse($escuela->toArray(),'Escuela suprimida con éxito');
    }
}

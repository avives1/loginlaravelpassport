<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use App\Models\Profesor;
use App\Models\Perfil;

class ProfesorController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //para verlos todos
        $profesores = Profesor::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($profesores->toArray(),
                'Profesores recibidos correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recogemos ALUMNO
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'apellidos'=> 'required',
            'email'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $profesor= Profesor::create($input);
        
        return $this->sendResponse($profesor->toArray(),
                'Profesor creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $profesor=Profesor::find($id);
        
        if(is_null($profesor)){
            return $this->sendError('Profesor no encontrado.');
        }
        
        return $this->sendResponse($profesor->toArray(), 'Alumno recibido con éxito.');
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profesor $profesor)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'apellidos'=> 'required',
            'email'=> 'required',
            'localidad'=> 'required'
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.',
                    $validator->errors());
        }
        
        $profesor->nombre=$input['nombre'];
        $profesor->apellidos=$input['apellidos'];
        $profesor->telefono=$input['telefono'];
        $profesor->email=$input['email'];
        $profesor->dni=$input['dni'];
        $profesor->fecha_nacimiento=$input['fecha_nacimiento'];
        $profesor->localidad=$input['localidad'];
        $profesor->imagen=$input['imagen'];
        $profesor->save();
        
        return $this->sendResponse($profesor->toArray(),'Profesor actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profesor $profesor)
    {
        $profesor->delete();
        
        return $this->sendResponse($profesor->toArray(),'Profesor suprimido con éxito');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use App\Models\Formacion;
use App\Models\Alumno;

class FormacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //para verlos todos
        $formacion = Formacion::all(); 
        
        //devuelve UN ARRAY PARA ORGANIZAR EN EL FRONTAL
        return $this->sendResponse($formacion->toArray(),
                'Formaciones recibidas correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recogemos ALUMNO
        $input=$request->all();
        //VALIDAMOS los atributos NOT NULL REQUIRED
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'fecha_inicio'=> 'required', 
            'fecha_fin'=> 'required',
            'centro_formativo'=> 'required',
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        $formacion=Formacion::create($input);
        
        return $this->sendResponse($formacion->toArray(),
                'Formacion creada con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formacion=Formacion::find($id);
        
        if(is_null($formacion)){
            return $this->sendError('Formacion no encontrada.');
        }
        
        return $this->sendResponse($formacion->toArray(), 'Formacion recibida con éxito.');
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formacion $formacion)
    {
        $input = $request->all();
        $validator = Validator::make($input,[
            'nombre'=> 'required',
            'fecha_inicio'=> 'required', 
            'fecha_fin'=> 'required',
            'centro_formativo'=> 'required',
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validación Error.', $validator->errors());
        }
        
        $formacion->nombre=$input['nombre'];
        $formacion->fecha_inicio=$input['fecha_inicio'];
        $formacion->fecha_fin=$input['fecha_fin'];
        $formacion->centro_formativo=$input['centro_formativo'];
        $formacion->nota_media=$input['nota_media'];
        $formacion->informacion_adicional=$input['informacion_adicional'];
        $formacion->save();
        
        return $this->sendResponse($formacion->toArray(),'Formacion actualizada con éxito.');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formacion $formacion)
    {
        $formacion->delete();
        
        return $this->sendResponse($formacion->toArray(),'Formacion suprimida con éxito');
    
    }
}

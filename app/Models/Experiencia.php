<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model
{
    protected $table = 'experiencia';
    protected $fillable =['ocupacion','descripcion','duracion','centro_educativo','profesor_id'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{       
    protected $table = 'alumnos';
    protected $fillable =['nombre','apellidos','telefono','email','dni',
        'fecha_nacimiento','localidad','imagen','curriculum', 'competencias','aptitudes', 'perfil_id'];
}

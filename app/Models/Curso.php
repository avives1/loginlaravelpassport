<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'cursos';
    protected $fillable =['nombre','descripcion','categoria','plazas','fecha_inicio',
        'fecha_fin','estado','imagen', 'escuela_id'];
}

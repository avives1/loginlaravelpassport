<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formacion extends Model
{
    protected $table = 'formacion';
    protected $fillable = ['nombre','fecha_inicio','fecha_fin',
        'centro_educativo','nota_media','informacion_adicional', 'alumno_id'];
}

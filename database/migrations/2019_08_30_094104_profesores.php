<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Profesores extends Migration
{
    
    public function up()
    {
        Schema::create('profesores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 50);
            $table->string('apellidos', 100);
            $table->string('telefono', 9)->nullable();
            $table->string('email', 100);
            $table->string('dni', 9)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('localidad', 100);
            $table->string('imagen', 255)->nullable();
            $table->integer('perfil_id')->unsigned();
            $table->timestamps();
            $table->foreign('perfil_id')->references('id')->on('perfiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

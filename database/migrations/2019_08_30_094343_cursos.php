<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cursos extends Migration
{
    
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('descripcion', 255);
            $table->string('categoria', 255);
            $table->string('localidad', 255);
            $table->integer('plazas')->nullable();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->integer('estado')->nullable();
            $table->string('imagen', 255)->nullable();
            $table->integer('escuela_id')->unsigned();
            $table->timestamps();
            $table->foreign('escuela_id')->references('id')->on('escuelas')->onDelete('cascade');
        });
    }

    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Experiencia extends Migration
{
   
    public function up()
    {
        Schema::create('experiencia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ocupacion', 255);
            $table->string('descripcion', 255)->nullable();
            $table->integer('duracion');
            $table->string('centro_educativo', 255);
            $table->integer('profesor_id')->unsigned();
            $table->timestamps();
            $table->foreign('profesor_id')->references('id')->on('profesores')->onDelete('cascade');
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

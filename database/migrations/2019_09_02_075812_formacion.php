<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Formacion extends Migration
{
    public function up()
    {
      Schema::create('formacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 255);
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('centro_formativo', 255);
            $table->integer('nota_media')->nullable();
            $table->string('informacion_adicional', 255)->nullable();
            $table->integer('alumno_id')->unsigned();
            $table->timestamps();
            $table->foreign('alumno_id')->references('id')->on('alumnos')->onDelete('cascade');
        });  
    }

    public function down()
    {
        //
    }
}

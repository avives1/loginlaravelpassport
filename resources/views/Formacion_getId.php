<?php

//inicio metodo curl
$ch = curl_init();

//coger el ID del que queramos encontrar
//añadirlo al final de la url
//
//declaramos la URL de la api
curl_setopt($ch, CURL_URL, "http://localhost:8000/api/formaciones/"
                                /* + variable ID que queramos comprobar */);

$headers =array(
    'Accept: application/json',
    'Authorization: Bearer '.$data2['access_token']
);
curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

//guardamos los datos recuperados en un string
$res = curl_exec($ch);

//imprimimos
var_dump($res);

//codificamos a Array de JSON
echo'<H3>Json_encode a array de GET API </H3>';
$data3= json_decode($res,true);
var_dump($data3);

//recuperamos valor de NAME por ejemplo:
echo'<H3> Valor NAME de GET API </H3>';
var_dump($data3['data']['name']);

//cerramos conexion
curl_close($ch);
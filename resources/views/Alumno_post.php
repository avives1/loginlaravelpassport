<?php

//api Url
$url = 'http://localhost:8000/api/alumnos';

//iniciamos cUrl
$ch = curl_init($url);

$headers = array(
    'Accept: application/json',
    'Authorization: Bearer '.$data2['acces_token']
);

//Datos para añadir al JSON
$jsonData = array(
    'nombre'=> /* se pone el nombre */'',
    'apellidos'=>/* se pone el nombre */'',
    'telefono'=>/* se pone el nombre */'',
    'email'=>/* se pone el nombre */'',
    'dni'=>/* se pone el nombre */'',
    'fecha_nacimiento'=>/* se pone el nombre */'',
    'localidad'=>/* se pone el nombre */'',
    'imagen'=>/* se pone el nombre */'',
    'curriculum'=>/* se pone el nombre */'',
    'competencias'=>/* se pone el nombre */'',
    'aptitudes'=>/* se pone el nombre */'',
);

//codificamos los datos en el JSON
$jsonDataEncoded=json_encode($jsonData);
var_dump($jsonDataEncoded);/*Muestra info del json encodificado*/
//Queremos hacer un POST por Curl
curl_setopt($ch,CURLOPT_POST,1);

//Adjuntamos el ARRAY del JSONDATA en los campos del POST
curl_setopt($ch,CURLOPT_POSTFIELDS,$jsonData);

//"set the content type to application/Json"
curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);

//ejecutamos la peticion/request
$result= curl_exec($ch);

//cerramos conexion
curl_close($ch);